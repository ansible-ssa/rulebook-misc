# rulebook-misc

The rulebooks provided in this repository are for testing purposes and to demonstrate the capabilities of [Event-Driven Ansible](https://www.ansible.com/blog/getting-started-with-event-driven-ansible).

You can run the rulebooks locally with the [ansible-rulebook](https://ansible.readthedocs.io/projects/rulebook/en/stable/index.html) command or in the [Red Hat Ansible Automation Platform](https://www.redhat.com/de/technologies/management/ansible) from the [EDA Controller](https://access.redhat.com/documentation/en-us/red_hat_ansible_automation_platform/2.4/html/event-driven_ansible_controller_user_guide/index).

## Testing locally

Create a variable file with your AWS credentials:

```yaml
access_key: "< your AWS key>"
secret_key: "< your AWS secret>"
```

Start the rulebook on the command line:

```bash
ansible-rulebook -r rulebooks/rulebook-cloudtrail.yml --controller-url=<FQDN of your automation controller> --controller-token=<controller authentication token> -v --print-events -e vars.yml
```
